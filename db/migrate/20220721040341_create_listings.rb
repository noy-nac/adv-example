class CreateListings < ActiveRecord::Migration[7.0]
  def change
    create_table :listings do |t|
      t.binary :buysell
      t.string :product
      t.string :price
      t.string :contacts
      t.text :description
      t.string :password

      t.timestamps
    end
  end
end
